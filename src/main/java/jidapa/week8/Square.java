package jidapa.week8;

public class Square {
    private int width;
    private int height;
    private int area;

    public Square(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int printSquareArea() {
        area = width * height;
        System.out.println(area);
        return area;
    }

}
